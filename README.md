# taskManagement

# Task Management SaaS

## Requirements

- Elixir 1.11+
- Phoenix 1.5+
- Erlang 23+

## Setup

Clone the repository:

```sh
git clone <repo_url>
cd task_management


#Install dependencies:
mix deps.get


#Start the server:
mix phx.server


The application will be available at http://localhost:4000.


#Create Task
POST /api/users/:user_id/tasks
{
  "task": {
    "title": "New Task",
    "description": "Task description",
    "due_date": "2021-06-25T12:34:56",
    "status": "To Do"
  }
}

#Update Task

PUT /api/users/:user_id/tasks/:id
{
  "task": {
    "title": "Updated Task",
    "description": "Updated description",
    "due_date": "2021-06-26T12:34:56",
    "status": "Done"
  }
}




